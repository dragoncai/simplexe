using namespace std;
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#ifndef MATRICEA_HPP_
#define MATRICEA_HPP_

/** \class MatriceA
 * \brief Classe représentant la matrice A du problème
 * 
 * Cette classe est composée des coefficients des contraintes du problème d'optimisation.
*/

class MatriceA : public vector<float>{
private :
	int nbLignes; /**< Nombre de lignes correspondant au nombre de contraintes. */
	int nbColonnes; /**< Nombre de colonnes correspondant au nombre de variables (y compris les variables d'écarts et fictives). */

public :
	/** \fn MatriceA
	 * \brief Constructeur par défaut.
	 */
	MatriceA();

	/** 
	 * \brief Constructeur paramétré/
	 * \param nbContraintes Nombre de contraintes.
	 * \param nbVariables Nombre de variables (d'écarts et fictives comprises).
	 */
	MatriceA(int nbContraintes, int nbVariables);

	/** \fn getNbLignes
	 * \brief Accesseur pour le nombre de lignes;
	 */
	int getNbLignes();

	/** \fn getNbColonnes
	 * \brief Accesseur pour le nombre de colonnes;
	 */
	int getNbColonnes();

	/** \fn get
	 * \brief Récupère une composante de la matrice.
	 * \param i Indice de ligne.
	 * \param j Indice de colonne.
	 */
	float get(int i, int j);

	/** \fn afficherMatrice
	 * \returns Une chaine de caractère représentant la matrice est ses composants.
	 */
	string afficherMatrice();

	/** \fn set
	 * \brief Modifie une composante de la matrice
	 * \param i Indice de ligne
	 * \param j Indice de colonne
	 * \param x Nouvelle valeur
	 */
    	void set(int i, int j, float x);
};


#endif /* MATRICEA_HPP_ */
