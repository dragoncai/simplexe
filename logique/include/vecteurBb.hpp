using namespace std;
#include <vector>
#include "variable.hpp"
#ifndef VECTEURBB_HPP_
#define VECTEURBB_HPP_

/** \class VecteurBb
 * \brief Classe représentant le vecteur Bb
 * 
 */

class VecteurBb : public vector<Variable> {
private:

public:
	/** \fn get
	 * \brief Récupère une composante du vecteur Bb
	 * \param i Indice de ligne
	 */
	float get(int i);
	
	/** \fn getIndiceVariable
	 * \brief Récupère l'indice de la variable (ie x_(i)) qui correspond à la composante du vecteur (ie Bb_(i)).
	 * param i Indice de ligne
	 */
	int getIndiceVariable(int i);
	
	/** \fn isPrimalReal
	 * \brief Détermine si le problème est primal réalisable.
	 * \returns TRUE si tous les composates sont positives, FALSE sinon.
	 */
	bool isPrimalReal();
	
	/** \fn plusNegatif
	 * \brief Détermine la valeur la plus négative.
	 * \returns La composante la plus négative du vecteurBb.
	 */
	float plusNegatif();
	
	/** \fn indexOf
	 * \brief Détermine l'indice d'une composante du vecteur.
	 * \param x Composante du vecteur
	 * \returns L'indice de la composante recherchée.
	 */
	int indexOf(float x);
	
	/** \fn set
	 * \brief Modifie la valeur d'une composante.
	 * \param i Indice de ligne
	 * \param x La nouvelle valeur
	 */
    void set(int i, float x);
	
	/** \fn setIndice
	 * \brief Modifie la variable x_(i) associée à la composante Bb_(i).
	 * \param i Indice de ligne
	 * \param indice Nouvelle indice pour la variable
	 */
	void setIndice(int i, int indice);
};



#endif
