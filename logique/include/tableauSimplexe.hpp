using namespace std;
#include <vector>

#ifndef TABLEAUSIMPLEXE_HPP_
#define TABLEAUSIMPLEXE_HPP_

/** \class TableauSimplexe
 * \brief Classe représentant le tableau du simplexe
 * 
 * Cette classe contient les trois classes suivante : MatriceA, VecteurBb et VecteurD. Elle utilisera la méthode du pivot de Gauss et calculera le cout minimal avec la solution optimale.
 */
class TableauSimplexe {

private :
	MatriceA A; /**< Matrice A du tableau simplexe*/
	VecteurBb Bb; /**< Vecteur Bb du tableau simplexe*/
	VecteurD D; /**< Vecteur d du tableau simplexe*/
	float cout; /**< Coût du tableau simplexe */
	float pivot; /**< Variable représentant la valeur de la composante de la matrice A qui sert de pivot de Gauss */
	int iPivot; /**< Indice de ligne du pivot */
	int jPivot; /**< Indice de colonne du pivot */


public :

	/** \fn TableauSimplexe
	 * \brief Construit un TableauSimplexe
	 * \param M MatriceA
	 * \param N VecteurBb
	 * \param P VecteurD
	 * \param C Coût du probleme
	 */
	TableauSimplexe(MatriceA M, VecteurBb N, VecteurD P, float C);

	/** \fn getA
	 * \brief Accesseur MatriceA
	 */
	MatriceA getA();
	
	/** \fn getBb
	 * \brief Accesseur VecteurBb
	 */
	VecteurBb getBb();
	
	/** \fn getD
	 * \brief Accesseur VecteurD
	 */
	VecteurD getD();
	
	/** \fn getCout
	 * \brief Accesseur cout
	 */
	float getCout();
	
	/** \fn chercherPivot
	 * \brief Détermine le pivot de Gauss
	 * Attribue une valeur à la variable pivot. Le pivot est choisi via l'indice de colonne de la composante la plus négative du VecteurD.
	 * \returns L'indice du pivot de Gauss.
	 */
	int chercherPivot();
	
	/** \fn solutionTrouvee
	 * \brief Détermine si la solution est optimale
	 * \returns TRUE si la solution est optimale, FALSE sinon.
	 */
	bool solutionTrouvee();
	
	/** \fn calculSimplexe
	 * \brief Calcule un nouveau tableau du simplexe.
	 * Un nouveau tableau du simplexe est calculé via la méthode du pivot de Gauss.
	 */
	void calculSimplexe();
	
	/** \fn getPivot
	 * \brief Accesseur pivot
	 */
	float getPivot();
	
	/** \fn getIPivot
	 * \brief Accesseur iPivot
	 */
	int getIPivot();
	
	/** \fn getJPivot
	 * \brief Accesseur jPivot
	 */
	int getJPivot();
	
	friend ostream & operator << (ostream&, TableauSimplexe&);
	/*! 
	 * \brief Surcharge operateur
	 * Affiche le tableau du simplexe.
	 */
};


#endif /* TABLEAUSIMPLEXE_HPP_ */
