#ifndef CONTRAINTE_H
#define CONTRAINTE_H

/** \struct Contrainte
 * \brief Représente une contrainte de probleme d'optimisation.
 */
struct Contrainte{
	vector<float> coef; /*!< Vector représentant les coefficients de l'inéquation/équation de la forme a*x_1 + b*x_2 + ... */
	char inegalite; /*!< Caractère correspondant au signe de l'inequation/equation. ('<', '>' ou '=') */
	float secondMembre; /*!<Valeur correspondant au second membre de l'inéquation/equation */
};
#endif
