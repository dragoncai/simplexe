#ifndef PROBLEME_H
#define PROBLEME_H
#include "contrainte.hpp"

/** \struct Probleme
 * \brief Représente un probleme d'optimisation.
 */
struct Probleme{
	int minoumax; /*!< 1 si min, -1 si max*/
	vector<float> coef; /*!< Vector représentant les coefficients de l'inéquation/équation de la forme a*x_1 + b*x_2 + ...*/
	float leCout; /*!< Coût du probleme */
	vector<Contrainte> contraintes; /*!< Liste des contraintes*/
	int nbVariables; /*!< Nombre de variables initiales*/
	int nbVariablesEcarts; /*!< Nombre de variables d'écarts*/
	int nbVariablesFictives; /*!< Nombre de variables fictives*/
	int nbContraintes; /*!< Nombre de contraintes*/
};
#endif
