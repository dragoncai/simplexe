#ifndef VARIABLE_HPP__
#define VARIABLE_HPP__
/** \struct Variable
 * \brief Représente une variable x_i
 *
 * Variable est utilisée pour le VecteurBb. Les composantes du VecteurBb possèdent une valeur et une l'indice de la variable x_(i) qui lui correspond.
 */
struct Variable  {
            float valeur; /*!< Valeur */
            int indice_variable; /*!< Indice de la variable x_(i) associé à la valeur*/
};
#endif
