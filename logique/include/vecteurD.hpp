#include <vector>
using namespace std;

#ifndef VECTEURD_HPP_
#define VECTEURD_HPP_

/** \class VecteurD
 * \brief Classe qui représente le vecteur d
 */
class VecteurD : public vector<float> {
private:

public:
    /** \fn get
	 * \brief Récupère une composante du vecteur d.
     * \param i Indice de colonne
	 */
	float get(int i);
    
    /** \fn isDualReal
	 * \brief Détermine si le problème est dual réalisable.
     * \returns TRUE si toutes les composantes sont positives, FALSE sinon.
	 */
	bool isDualReal();
	
    /** \fn plusNegatif
	 * \brief Détermine la valeur la plus négative.
	 * \returns La composante la plus négative du vecteur d.
	 */
    float plusNegatif();
	
    /** \fn indexOf
	 * \brief Détermine l'indice d'une composante du vecteur.
	 * \param x Composante du vecteur
	 * \returns L'indice de la composante recherchée.
	 */
    int indexOf(float x);
    
    /** \fn set
	 * \brief Modifie la valeur d'une composante.
	 * \param i Indice de ligne
	 * \param x La nouvelle valeur
	 */
    void set(int i, float x);
};


#endif /* VECTEURD_HPP_ */
