/*
 * matriceA.cpp
 *
 *  Created on: 5 févr. 2014
 *      Author: ycai
 */
using namespace std;
#include <iostream>
#include <vector>
#include <string>
#include "matriceA.hpp"
#include <sstream>
template <typename T> string tostr(const T& t) {
	ostringstream os; os<<t; return os.str();
}

MatriceA::MatriceA(){
	nbLignes =0;
	nbColonnes = 0;
}

MatriceA::MatriceA(int nbContraintes, int nbVariables){
	nbLignes = nbContraintes;
	nbColonnes = nbVariables;

}

int MatriceA::getNbLignes(){
	return nbLignes;
}

int MatriceA::getNbColonnes(){
	return nbColonnes;
}

float MatriceA::get(int i, int j){
	return at((i-1)*getNbColonnes() + (j-1));
}

string MatriceA::afficherMatrice(){
	string a;
	for (int i=1; i<=getNbLignes(); i++)
		{
			for (int j=1; j<=getNbColonnes(); j++)
			{
				a +=  tostr(get(i,j)) + "\t";
			}
			a += "\n";
		}
	return a;
}

void MatriceA::set(int i, int j, float x){
    at(getNbColonnes()*(i-1) + (j-1)) = x;
}

