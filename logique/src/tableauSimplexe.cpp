#include "matriceA.hpp"
#include "vecteurBb.hpp"
#include "vecteurD.hpp"
#include "tableauSimplexe.hpp"
using namespace std;
#include <iostream>
#include <vector>
#include <string>
#include <sstream>


MatriceA TableauSimplexe::getA(){
return A;

}

VecteurBb TableauSimplexe::getBb(){

return Bb;
}

VecteurD TableauSimplexe::getD(){
return D;
}

float TableauSimplexe::getCout(){
	return cout;
}

float TableauSimplexe::getPivot(){
    return pivot;
}

int TableauSimplexe::getIPivot(){
    return iPivot;
}
int TableauSimplexe::getJPivot(){
    return jPivot;
}

/**
  * retourne l'indice du pivot dans le vector
  * permet de chercher le pivot et attribue une valeur à "pivot", "iPivot" et "jPivot"
  */
int TableauSimplexe::chercherPivot(){
      //Si le probleme est primal realisable
	if (Bb.isPrimalReal()){
		int indiceColonne, indiceLigne;
		indiceColonne = D.indexOf(D.plusNegatif());
		float temp=-1;
		for(int i=1 ; i<=A.getNbLignes() ; i++){
			if (A.get(i,indiceColonne) > 0)
			{
				if (temp < 0 || Bb.get(i)/A.get(i, indiceColonne)<temp)
				{
 					temp = Bb.get(i)/A.get(i, indiceColonne);
 					indiceLigne = i;
				}
			}
		}
            iPivot = indiceLigne;
            jPivot = indiceColonne;
            pivot = A.get(iPivot,jPivot);
		return ((indiceLigne-1)*A.getNbColonnes() + (indiceColonne-1));
	}
      //si le probleme est dual realisable
	else if(D.isDualReal())
	{
		int indiceLigne, indiceColonne;
		indiceLigne=Bb.indexOf(Bb.plusNegatif());
		float temp=-1;

		for(int i=1 ; i<=A.getNbColonnes() ; i++){
			if (A.get(indiceLigne,i) < 0)
			{
				if (temp < 0 || (-1)*D.get(i)/A.get(indiceLigne, i)<temp)
				{
					temp = D.get(i)/A.get(indiceLigne, i);
					indiceColonne=i;
				}
			}
		}
            iPivot = indiceLigne;
            jPivot = indiceColonne;
            pivot = A.get(iPivot,jPivot);
		return ((indiceLigne-1)*A.getNbColonnes() + (indiceColonne-1));
	}
	else
		return -1;
}


/**
  * initialise le tableau par une matrice A, vecteur Bb, vecteur D et un cout
  */
TableauSimplexe::TableauSimplexe(MatriceA M, VecteurBb N, VecteurD P, float C){
	A=M;
	Bb=N;
	D=P;
	cout=C;
}

/**
  * retourne TRUE si le problème est dual realisable et primal realisable
  */
bool TableauSimplexe::solutionTrouvee()
{
	return (D.isDualReal() && Bb.isPrimalReal());
}

/**
  *   application de la methode du pivot de Gauss
  */
void TableauSimplexe::calculSimplexe()
{
      // \param coef == coefficient pour la combinaison linéaire
      float coef;
      // Mis a jour du nouveau pivot
      chercherPivot();
        
	// division de la ligne iPivot par la variable pivot
	for (int j=1; j<= A.getNbColonnes(); j++){
	      A.set(iPivot, j, (A.get(iPivot,j) / pivot) );
	}
	Bb.set(iPivot, Bb.get(iPivot)/pivot);
	Bb.setIndice(iPivot, jPivot);
       
	// Modification du tableau en entier
      // L_i = L_i - L_iPivot*coef
	for (int i=1; i<= A.getNbLignes() ; i++){
		if (i!=iPivot){
			coef = A.get(i,jPivot);
			for(int j=1; j<=A.getNbColonnes(); j++){
				A.set(i,j, A.get(i,j) - A.get(iPivot,j)*coef);
			}
			
			Bb.set(i, Bb.get(i) - Bb.get(iPivot)*coef);
		}
	}
      // Meme chose pour le vecteur D
	coef = D.get(jPivot);
	for(int j=1; j<=A.getNbColonnes(); j++){
		D.set(j, D.get(j) - A.get(iPivot,j)*coef);
	}
      // Ainsi que le cout
	cout = cout - Bb.get(iPivot)*coef;


}

/**
  * Surchage de l'operateur <<
  * permet l'affichage du tableau du simplexe
  */
ostream & operator << (ostream &s, TableauSimplexe &tab){
	
      for (int i=1; i<= tab.getA().getNbColonnes() ; i++)
      {
              s << "x_" << i << "\t";
      }
      s << endl;


      for (int i=1; i<= tab.getA().getNbLignes() ; i++)
	{
            //affichage des coefficient de la matrice A a la ligne i
		for (int j=1; j<= tab.getA().getNbColonnes(); j++)
		{
			s << tab.getA().get(i,j) << "\t";
		}
            //affichage de l'element i du vecteur Bb
		s << "|" << tab.getBb().get(i) << "\t x_" << tab.getBb().getIndiceVariable(i) << "\n";
	}

      //délimitation du tableau en fonction du nombre de colonnes
	for (int j=1; j<= tab.getA().getNbColonnes(); j++)
	{
		s << "________";
	}
	s << "____" << endl;

      //affichage du vecteur D
	for (int j=1; j<= tab.getA().getNbColonnes(); j++)
	{
		s << tab.getD().get(j) << "\t";
	}

      //affichage du cout
	s << "|" << tab.getCout() << "\n" << "===================================" << endl;
}


/*
int main(){

	MatriceA A(2,2);
	A.push_back(1);
	A.push_back(2);
	A.push_back(3);
	A.push_back(4);

	VecteurBb Bb;
	Bb.push_back(2);
	Bb.push_back(5);

	VecteurD D;
	D.push_back(-6);
	D.push_back(-2);

	TableauSimplexe caca(A,Bb,D,0);

	cout << caca;

	while (!caca.solutionTrouvee())
	{

		caca.calculSimplexe();
		cout << caca;
	}
	


}*/
