/*
 * vecteurD.cpp
 *
 *  Created on: 5 févr. 2014
 *      Author: sleveugle
 */


#include "vecteurD.hpp"
#include <vector>
#include<string>
#include <iostream>
using namespace std;

float VecteurD::get(int i){
	return this->at(i-1);
}

float VecteurD::plusNegatif(){
	float temp=0;
	for(int i=1 ; i<=this->size() ; i++){
		if (get(i)<temp){
			temp=get(i);
		}
	}
	return temp;
}

int VecteurD::indexOf(float x){
	for (int i=1; i<= size(); i++)
	{
		if (get(i)==x)
		{
			return i;
		}
	}
	return -1;
}

bool VecteurD::isDualReal(){
	for (int i=1 ; i<= this->size() ; i++){
		if(get(i) < 0)
			return false;
	}
	return true;
}

void VecteurD::set(int i, float x){
    at(i-1)=x;
}

/*
int main(){

	VecteurD D;
	D.push_back(1);
	D.push_back(2);
	D.push_back(3);

	for (int i=1; i<= D.size(); i++){
		cout<<D.get(i)<< "\n";

	}

	if (D.isDualReal()==false){
		cout<<"le vecteur n'est pas dual realisable \n";
	}
	else { cout<<"le vecteur est dual realisable \n";

	}


}
*/
