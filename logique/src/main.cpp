using namespace std;
#include <iostream>
#include <vector>
#include <string>
#include "vecteurBb.hpp"
#include "vecteurD.hpp"
#include "matriceA.hpp"
#include "variable.hpp"
#include "tableauSimplexe.hpp"
#include "contrainte.hpp"
#include "probleme.hpp"
#define MAX 100

vector<Contrainte> recupererContraintes(int nbContraintes, int nbVariables)
{
        float x;
        vector<Contrainte> contraintes;
        
	for (int i=1; i<=nbContraintes; i++)
	{
                Contrainte c;
		cout << "contrainte n°" << i << " : \n";
		for (int j=1; j<=nbVariables; j++)
		{
                        cin >> x;
                        c.coef.push_back(x);

		}
                cin >> c.inegalite;
                cin >> c.secondMembre;
                contraintes.push_back(c);
	}
        return contraintes;
}

Probleme probleme(int nbVariables, int nbContraintes, int minoumax)
{
	Probleme p;
	p.minoumax = minoumax;
        p.nbVariables = nbVariables; 
        p.nbContraintes = nbContraintes;
        p.nbVariablesEcarts = 0;
        p.nbVariablesFictives = 0;
	float x;

        cout << "coefficient du probleme de la forme (a*x_1 + b*x_2 + .. + n*x_n) \n";

	for (int i=0; i< p.nbVariables; i++)
	{
		cin >> x;
		p.coef.push_back(x*minoumax);
	}
        cin >> x;
        p.leCout = x;
        
        p.contraintes = recupererContraintes(nbContraintes, nbVariables);

	for (int i=p.nbVariables; i< p.nbVariables+p.nbContraintes; i++)
	{
		p.coef.push_back(0);
	}
	return p;
}

bool possedeBaseRealisable(Probleme p)
{
	if (p.nbVariablesEcarts != p.nbContraintes)
	{
		return false;
	}
	else
	{
		int i=0;
		while(i< p.nbVariablesEcarts && p.contraintes.at(i).coef.at(p.nbVariables + i) != -1)
		{
			i++;
		}
		if (i < p.nbVariablesEcarts)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}

void analyseDuProbleme(Probleme &p)
{
        //VARIABLES D'ECARTS
        for(int i=0;i<p.nbContraintes;i++)
        {
                if (p.contraintes.at(i).inegalite == '<')
                {
                        p.contraintes.at(i).coef.push_back(1);
                        p.nbVariablesEcarts++;
                        for (int j=0; j< p.nbContraintes; j++)
                        {
                                if (i!=j)
                                {
                                        p.contraintes.at(j).coef.push_back(0);
                                }
                        }
                        p.contraintes.at(i).inegalite = '=';
			p.coef.push_back(0);
                }
                else if (p.contraintes.at(i).inegalite == '>')
                {
                        p.contraintes.at(i).coef.push_back(-1);
                        p.nbVariablesEcarts++;
                        for (int j=0; j< p.nbContraintes; j++)
                        {
                                if (i!=j)
                                {
                                        p.contraintes.at(j).coef.push_back(0);
                                }
                        }
                        p.contraintes.at(i).inegalite = '=';
                }

                if (p.contraintes.at(i).secondMembre < 0)
                {
                        for (int j=0; j< p.nbVariables + p.nbVariablesEcarts; j++)
                        {
                                p.contraintes.at(i).coef.at(j) = p.contraintes.at(i).coef.at(j) * -1;
                        }
			p.contraintes.at(i).secondMembre = p.contraintes.at(i).secondMembre * -1;
                }
        }

        //VARIABLES FICTIVES
        if (!possedeBaseRealisable(p))
        {
                for(int i=0;i<p.nbContraintes;i++)
                {
                        
                        p.contraintes.at(i).coef.push_back(1);
                        p.nbVariablesFictives++;
                        for (int j=0; j< p.nbContraintes; j++)
                        {
       		                if (i!=j)
                                {
                                        p.contraintes.at(j).coef.push_back(0);
                                }
                        }
                }
	}
	
       
}

void resolutionDuProbleme(Probleme p)
{
        MatriceA A(p.nbContraintes, p.nbVariables + p.nbVariablesEcarts + p.nbVariablesFictives);
        VecteurBb Bb;
        VecteurD D;
	float leCout=0;
        
        for (int i=0; i<A.getNbLignes(); i++)
        {
                for (int j=0; j<A.getNbColonnes(); j++)
                {
                        A.push_back(p.contraintes.at(i).coef.at(j));
                }
                Variable V;
		V.valeur = p.contraintes.at(i).secondMembre;
		V.indice_variable = p.nbVariables + p.nbVariablesEcarts + p.nbVariablesFictives - p.nbContraintes + i + 1;
		Bb.push_back(V);
        }

	if (p.nbVariablesFictives > 0)
	{
		for (int j=0; j<A.getNbColonnes(); j++)
		{
			float x=0;
			for (int k=0; k<A.getNbLignes(); k++)
			{
				x += A.get(k+1,j+1);
			}
			if (j<p.nbVariables+p.nbVariablesEcarts)
				D.push_back(x*-1);
			else
				D.push_back(0);
		}

		for (int k=0; k<A.getNbLignes(); k++)
		{
			leCout += Bb.get(k+1);
		}
		leCout = leCout*-1;
	}
	else
	{
		for (int j=0; j<A.getNbColonnes(); j++)
		{
			D.push_back(p.coef.at(j));
		}
		leCout = p.leCout;
	}

	
	TableauSimplexe T(A,Bb,D,leCout);
	cout << T;
	while (!T.solutionTrouvee())
	{
		T.calculSimplexe();
		cout.precision(3);
		cout << T;
	}

	if (p.nbVariablesFictives > 0)
	{
		MatriceA A2(p.nbContraintes, p.nbVariables + p.nbVariablesEcarts);
		for (int i=0; i<A2.getNbLignes(); i++)
		{
			for (int j=0; j<A2.getNbColonnes(); j++)
			{
				A2.push_back(T.getA().get(i+1,j+1));
			}
		}
		VecteurBb Bb2 = T.getBb();
		
		//calcul de la fonction cout à partir de la solution réalisable trouvé précédemment.
		
		VecteurD D2;
   
		for (int j=0; j<A2.getNbColonnes(); j++)
		{
			D2.push_back(p.coef.at(j));
		}

		for (int i=0; i<p.nbContraintes; i++)
		{
			for (int j=0; j<p.nbVariables+p.nbVariablesEcarts; j++)
			{
				if (j!=Bb2.getIndiceVariable(i+1) - 1)
				{
					D2.at(j) += A2.get(i+1,j+1)*p.coef.at(Bb2.getIndiceVariable(i+1)-1)*-1;
				}
				else
				{
					D2.at(j)=0;
				}
			}
		}

		float leCout2 = 0;
		for (int i=0; i<p.nbContraintes; i++)
		{
			leCout2 += Bb2.get(i+1)*p.coef.at(Bb2.getIndiceVariable(i+1)-1)*-1;
		}
		
		TableauSimplexe T(A2,Bb2,D2,leCout2);
		cout << T;
		while (!T.solutionTrouvee())
		{
			T.calculSimplexe();
			cout.precision(3);
			cout << T;
		}

	}
}

int main(int argc, char *argv[])
{
	int choix;
	int nbVariables;
	int nbContraintes;
	float solution[MAX];
	Variable y;
        vector<Contrainte> contraintes;

	cout << "1. MIN \t 2.MAX \n";
	cin >> choix;

	cout << "nombre de variables ?";
	cin >> nbVariables;

		
	cout << "nombre de contraintes ?";
	cin >> nbContraintes;
	
	MatriceA A( nbContraintes , nbVariables + nbContraintes);

	switch (choix)
	{
		case 1 : {
			cout << "min";
			cout << endl;
			break;
		}
		case 2 : {
			cout << "max";
			cout << endl;
			choix = -1;
			break;
		}
	}
		
	VecteurD D;
	VecteurBb Bb;
	
        Probleme p= probleme(nbVariables, nbContraintes, choix);

        analyseDuProbleme(p);

	resolutionDuProbleme(p);
}
