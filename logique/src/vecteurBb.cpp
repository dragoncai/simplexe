#include "vecteurBb.hpp"
#include "variable.hpp"
#include <vector>
#include<string>
#include <iostream>
using namespace std;


float VecteurBb::get(int i){
	return at(i-1).valeur;
}

int VecteurBb::getIndiceVariable(int i){
	return at(i-1).indice_variable;
}

float VecteurBb::plusNegatif(){
	float temp=0;
	for(int i=1 ; i<= size() ; i++){
		if (get(i)<temp){
			temp=get(i);
		}
	}
	return temp;
}

int VecteurBb::indexOf(float x){
	for (int i=1; i<= size(); i++)
	{
		if (get(i)==x)
		{
			return i;
		}
	}
	return -1;
}



bool VecteurBb::isPrimalReal(){
	for (int i=1 ; i<= size() ; i++){
		if(get(i) < 0)
			return false;
	}
	return true;
}

void VecteurBb::set(int i, float x){
    	at(i-1).valeur = x;

}

void VecteurBb::setIndice(int i, int indice){
    	at(i-1).indice_variable = indice;
}

/*
int main(){

	VecteurBb Bb;
	Bb.push_back(1);
	Bb.push_back(-2);
	Bb.push_back(3);

	for (int i=1; i<= Bb.size(); i++){
		cout<<Bb.get(i)<< "\n";

	}

	if (Bb.isPrimalReal()==false){
		cout<<"le vecteur n'est pas primal realisable \n";
	}
	else { cout<<"le vecteur est primal realisable \n";

	}


}
*/
