\contentsline {chapter}{\numberline {1}Hierarchical Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Class Hierarchy}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Contrainte Struct Reference}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Matrice\discretionary {-}{}{}A Class Reference}{5}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Detailed Description}{6}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Constructor \& Destructor Documentation}{6}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}Matrice\discretionary {-}{}{}A}{6}{subsubsection.3.2.2.1}
\contentsline {subsection}{\numberline {3.2.3}Member Function Documentation}{6}{subsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.3.1}afficher\discretionary {-}{}{}Matrice}{6}{subsubsection.3.2.3.1}
\contentsline {subsubsection}{\numberline {3.2.3.2}get}{6}{subsubsection.3.2.3.2}
\contentsline {subsubsection}{\numberline {3.2.3.3}set}{6}{subsubsection.3.2.3.3}
\contentsline {section}{\numberline {3.3}Probleme Struct Reference}{7}{section.3.3}
\contentsline {section}{\numberline {3.4}S\discretionary {-}{}{}Variable Struct Reference}{7}{section.3.4}
\contentsline {section}{\numberline {3.5}Tableau\discretionary {-}{}{}Simplexe Class Reference}{7}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Constructor \& Destructor Documentation}{8}{subsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.1.1}Tableau\discretionary {-}{}{}Simplexe}{8}{subsubsection.3.5.1.1}
\contentsline {subsection}{\numberline {3.5.2}Member Function Documentation}{8}{subsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.2.1}calcul\discretionary {-}{}{}Simplexe}{8}{subsubsection.3.5.2.1}
\contentsline {subsubsection}{\numberline {3.5.2.2}chercher\discretionary {-}{}{}Pivot}{8}{subsubsection.3.5.2.2}
\contentsline {subsubsection}{\numberline {3.5.2.3}solution\discretionary {-}{}{}Trouvee}{8}{subsubsection.3.5.2.3}
\contentsline {subsection}{\numberline {3.5.3}Friends And Related Function Documentation}{8}{subsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.3.1}operator$<$$<$}{8}{subsubsection.3.5.3.1}
\contentsline {section}{\numberline {3.6}Vecteur\discretionary {-}{}{}Bb Class Reference}{8}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Detailed Description}{9}{subsection.3.6.1}
\contentsline {section}{\numberline {3.7}Vecteur\discretionary {-}{}{}D Class Reference}{9}{section.3.7}
\contentsline {part}{Index}{10}{section*.10}
